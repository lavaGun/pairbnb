PairBnB
===

This is a small project, intended to improve and showcase my ionic abilities. I used the following course: https://www.udemy.com/share/101WUoAEYbdFpSRH8D/ for instruction. 

To run:
---


- Install ionic: https://ionicframework.com/docs/intro/cli

```
ionic serve
```
