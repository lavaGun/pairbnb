import { AppState, Plugins, Capacitor } from '@capacitor/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { Platform } from '@ionic/angular';
// import { SplashScreen } from '@ionic-native/splash-screen/ngx';
// import { StatusBar } from '@ionic-native/status-bar/ngx';
import { take } from 'rxjs/internal/operators/take';
// import { Plugins } from '@capacitor/core'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private authSub: Subscription;
  private previousAuthState = false;
  constructor(
    private platform: Platform,
    // private splashScreen: SplashScreen,
    // private statusBar: StatusBar,
    private authService: AuthService,
    private router: Router
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.authSub = this.authService.userIsAuthenticated.subscribe(isAuthenticated => {
      if (!isAuthenticated && this.previousAuthState !== isAuthenticated)
        this.router.navigateByUrl('/auth');
      this.previousAuthState = isAuthenticated;
    })

    Plugins.App.addListener('appStateChange', this.checkAuthOnResume.bind(this));
  }

  ngOnDestroy() {
    if (this.authSub)
      this.authSub.unsubscribe();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (Capacitor.isPluginAvailable('SplashScreen'))
        Plugins.SplashScreen.hide();
    });
  }

  checkAuthOnResume(state: AppState) {
    if (state.isActive) {
      this.authService.autoLogin()
        .pipe(take(1))
        .subscribe(success => {
          if (!success)
            this.onLogout();
        })
    }
  }

  onLogout() {
    console.log('logging out');
    this.authService.logout();
    this.router.navigateByUrl('/auth');
  }
}
