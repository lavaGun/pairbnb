export class User {
    constructor(
        public id: string,
        public emai: string,
        private _token: string,
        private tokenExpirationDate: Date
    ) { }

    get token() {
        if (!this.tokenExpirationDate || this.tokenExpirationDate <= new Date())
            return null;
        return this._token;
    }

    get tokenDuration() {
        if (!this.token)
            return null;
        return this.tokenExpirationDate.getTime() - new Date().getTime()
    }
}