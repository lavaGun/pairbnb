import { User } from './user.model';
import { Observable, BehaviorSubject } from 'rxjs';
// import { AuthResponseData } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { Plugins } from '@capacitor/core'
import { environment } from '../../environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { tap } from 'rxjs/internal/operators/tap';
import { from } from 'rxjs/internal/observable/from';

export interface AuthResponseData {
  kind: string;
  idToken: string;
  email: string;
  refreshToken: string;
  localId: string;
  expiresIn: string;
  registered?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {
  _user = new BehaviorSubject<User>(null);
  private _otherUserId = 'bcd';
  private activeLogoutTimer: any;

  get userId() {
    return this._user.asObservable().pipe(map(user => {
      if (user)
        return user.id
      else
        return null;
    }))
  }

  get secondaryUserId() { return this._otherUserId }

  get userIsAuthenticated() {
    return this._user.asObservable().pipe(
      map(user => {
        if (user)
          return !!user.token
        else
          return false;
      })
    )
  }

  get token() {
    return this._user.asObservable().pipe(map(user => {
      if (user)
        return user.token;
      else
        return null;
    }))
  }

  constructor(
    private http: HttpClient
  ) { }

  ngOnDestroy() {
    if (this.activeLogoutTimer)
      clearTimeout(this.activeLogoutTimer)
  }
  autoLogin() {
    return from(Plugins.Storage.get({ key: 'authData' })).pipe(
      map(storedData => {
        if (!storedData || !storedData.value)
          return null;
        const parsedData = JSON.parse(storedData.value) as { userId: string, token: string, tokenExpirationDate: string, email: string };

        const expirationTime = new Date(parsedData.tokenExpirationDate);
        if (expirationTime <= new Date())
          return null;

        return new User(parsedData.userId, parsedData.email, parsedData.token, expirationTime)
      }),
      tap(user => {
        if (user) {
          this._user.next(user);
          this.autoLogout(user.tokenDuration);
        }
      }),
      map(user => {
        return !!user;
      })
    )
  }

  private autoLogout(duration: number) {
    if (this.activeLogoutTimer)
      clearTimeout(this.activeLogoutTimer)
    setTimeout(() => {
      this.logout();
    }, duration)
  }

  signUp(email: string, password: string) {
    return this.http.post<AuthResponseData>(
      `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.firebaseApiKey}`,
      { email: email, password: password, returnSecureToken: true }
    ).pipe(tap(this.setUserData.bind(this)))
  }

  login(email: string, password: string): Observable<AuthResponseData> {
    return this.http.post<AuthResponseData>(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.firebaseApiKey}`,
      { email: email, password: password, returnSecureToken: true }
    ).pipe(tap(this.setUserData.bind(this)))
  }

  logout() {
    if (this.activeLogoutTimer)
      clearTimeout(this.activeLogoutTimer)
    this._user.next(null);
    Plugins.Storage.remove({ key: 'authData' });
  }

  private storeAuthData(userId: string, token: string, tokenExpirationDate: string, email: string) {
    const data = JSON.stringify({ userId: userId, token: token, tokenExpirationDate: tokenExpirationDate, email: email })
    Plugins.Storage.set({
      key: 'authData',
      value: data
    })
  }

  private setUserData(userData: AuthResponseData) {
    const expirationTime = new Date(new Date().getTime() + (+userData.expiresIn * 1000));
    const user = new User(
      userData.localId,
      userData.email,
      userData.idToken,
      expirationTime
    )
    this._user.next(user);
    this.autoLogout(user.tokenDuration);
    this.storeAuthData(userData.localId, userData.idToken, expirationTime.toISOString(), userData.email);
  }
}
