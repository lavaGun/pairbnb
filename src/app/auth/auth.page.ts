import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService, AuthResponseData } from './auth.service';
import { Component, OnInit } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  isLoading: boolean = false;
  isLogin: boolean = true;

  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
  }

  authenticate(email: string, password: string) {
    this.isLoading = true;
    this.loadingCtrl.create({
      keyboardClose: true, message: 'Logging in...'
    }).then(loadingEl => {
      loadingEl.present();
      let authObservable: Observable<AuthResponseData>;
      if (this.isLogin)
        authObservable = this.authService.login(email, password);
      else
        authObservable = this.authService.signUp(email, password);

      authObservable.subscribe(result => {
        this.isLoading = false;
        loadingEl.dismiss();
        this.router.navigateByUrl('/places/tabs/discover');
      }, errorResponse => {
        this.isLoading = false;
        console.log(errorResponse)
        loadingEl.dismiss();
        const code = errorResponse.error.error.message;
        let message = 'Could not sign up, please try again';
        if (code === 'EMAIL_EXISTS') {
          message = 'That email already exists!';
        } else if (code === 'EMAIL_NOT_FOUND')
          message = 'Email address could not be found!';
        else if (code === 'INVALID_PASSWORD')
          message = 'Invalid password!';
        this.showAlert(message);
      })
    })
  }

  private showAlert(message: string) {
    this.alertCtrl.create({
      message: message,
      header: 'Authentication Failed',
      buttons: ['Okay']
    }).then(alertEl => {
      alertEl.present();
    })
  }

  onLogin() {

  }

  onSwitchAuthMode() {
    this.isLogin = !this.isLogin;
  }

  onSubmit(form: NgForm) {
    if (!form.valid)
      return;

    const email = form.value.email;
    const password = form.value.password;

    this.authenticate(email, password);
    form.reset();
  }

}
