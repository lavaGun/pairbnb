import { Capacitor, Plugins } from '@capacitor/core';
import { PlaceLocation, Coordinates } from './../../../places/location.model';
import { map, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { MapModalComponent } from './../../map-modal/map-modal.component';
import { ModalController, ActionSheetController, AlertController } from '@ionic/angular';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { environment } from '../../../../environments/environment'
import { of } from 'rxjs/internal/observable/of';

const { Geolocation } = Plugins;

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss'],
})
export class LocationPickerComponent implements OnInit {
  @Output() locationPicked = new EventEmitter<PlaceLocation>();
  @Input() showPreview = false;
  selectedLocationImage: string;
  isLoading: boolean = false;

  constructor(
    private modalCtrl: ModalController,
    private http: HttpClient,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() { }

  onPickLocation() {
    console.log("Is this working?")
    this.actionSheetCtrl.create({
      header: 'Please Choose',
      buttons: [
        {
          text: 'Auto-Locate', handler: () => {
            this.locateUser();
          }
        },
        {
          text: 'Pick on Map', handler: () => {
            this.openMap();
          }
        },
        { text: 'Cancel', role: 'cancel' }
      ]
    }).then(actionSheetEl => actionSheetEl.present());
  }

  private getAddress(lat: number, lng: number) {
    return this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${environment.googleMapsAPIKey}`).pipe(
      map((geoData: any) => {
        console.log(geoData);
        if (!geoData || !geoData.results || geoData.results.length === 0)
          return null;
        return geoData.results[0].formatted_address;
      })
    );
  }

  private getMapImage(lat: number, lng: number, zoom: number) {
    return `https://maps.googleapis.com/maps/api/staticmap?center=${lat},${lng}&zoom=${zoom}&size=600x300&maptype=roadmap
    &markers=color:red%7Clabel:Place%7C${lat},${lng}&key=${environment.googleMapsAPIKey}`
  }

  private openMap() {
    this.modalCtrl.create({ component: MapModalComponent })
      .then(modalEl => {
        modalEl.onDidDismiss().then(locationData => {
          console.log(locationData.data);
          if (!locationData.data)
            return;
          const coordinates: Coordinates = {
            lat: locationData.data.lat,
            lng: locationData.data.lng
          };
          this.createPlace(coordinates.lat, coordinates.lng);
        });
        modalEl.present();
      });
  }

  private locateUser() {
    if (!Capacitor.isPluginAvailable('Geolocation')) {
      console.log('plugin not available');
      this.showErrorAlert('Plugin not available');
      return;
    } else {
      Geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(geoPosition => {
        const coordinates: Coordinates = { lat: geoPosition.coords.latitude, lng: geoPosition.coords.longitude };
        this.createPlace(coordinates.lat, coordinates.lng);
      }).catch(err => {
        console.log(err);
        this.showErrorAlert();
      })
    }

  }

  private showErrorAlert(method: string = 'catch') {
    this.alertCtrl.create({
      header: `Could not fetch location. ${method}`,
      message: 'Please use the map to pick a location!'
    }).then(alertEl => alertEl.present());
  }

  private createPlace(lat: number, lng: number) {
    const pickedLocation: PlaceLocation = {
      lat: lat,
      lng: lng,
      address: null,
      staticMapImageUrl: null
    };
    this.isLoading = true;
    this.getAddress(lat, lng).pipe(
      switchMap(address => {
        pickedLocation.address = address;
        return of(this.getMapImage(pickedLocation.lat, pickedLocation.lng, 14))
      })
    ).subscribe(staticMapImageUrl => {
      pickedLocation.staticMapImageUrl = staticMapImageUrl;
      this.selectedLocationImage = staticMapImageUrl;
      this.isLoading = false;
      this.locationPicked.emit(pickedLocation);
    });
  }
}
