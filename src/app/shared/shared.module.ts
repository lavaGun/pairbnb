import { ImagePickerComponent } from './pickers/image-picker/image-picker.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { MapModalComponent } from './map-modal/map-modal.component';
import { NgModule } from '@angular/core';
import { LocationPickerComponent } from './pickers/location-picker/location-picker.component';

@NgModule({
    imports: [CommonModule, IonicModule],
    declarations: [LocationPickerComponent, MapModalComponent, ImagePickerComponent],
    exports: [LocationPickerComponent, MapModalComponent, ImagePickerComponent]
})
export class SharedModule { }