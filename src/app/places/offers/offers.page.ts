import { delay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { PlacesService } from './../places.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Place } from '../place.model';
import { IonItemSliding } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
})
export class OffersPage implements OnDestroy, OnInit {
  offers: Place[];
  private placesSub: Subscription;
  isLoading = false;
  constructor(
    private placesService: PlacesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.placesSub = this.placesService.places.pipe(
      delay(1000)
    ).subscribe(places => {
      console.log(places);
      this.offers = places;
    });
  }

  ionViewWillEnter() {
    this.isLoading = true;
    this.placesService.fetchPlaces().subscribe(() => {
      this.isLoading = false;
    });
  }

  ngOnDestroy() {
    if (this.placesSub)
      this.placesSub.unsubscribe()
  }

  onEdit(offerId: string, slidingItem: IonItemSliding) {
    slidingItem.close();
    this.router.navigate(['/', 'places', 'tabs', 'offers', offerId])
  }

  //todo: add dummy offers, similar to dummy data for places
  //todo: tap dummy offer to navigate to offer details
  // display some bookings for the offer
}
