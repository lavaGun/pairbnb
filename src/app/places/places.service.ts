import { PlaceLocation } from './location.model';
import { AuthService } from './../auth/auth.service';
import { Place } from './place.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject, of } from 'rxjs';
import { take, map, tap, delay, mergeMap, switchMap } from 'rxjs/operators'
import { HttpClient } from '@angular/common/http';

interface PlaceData {
  availableFrom: string;
  availableTo: string;
  description: string;
  imageUrl: string;
  price: string;
  title: string;
  userId: string;
  location: PlaceLocation;
}

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  /**
   * [
    new Place(
      'p1',
      'NYC',
      'This is a description',
      'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/Lower_Manhattan_skyline_-_June_2017.jpg/1280px-Lower_Manhattan_skyline_-_June_2017.jpg',
      10,
      new Date(),
      new Date(),
      'xyz'
    ),
    new Place(
      'p2',
      'Paris',
      'This is a french description',
      'https://en.parisinfo.com/var/otcp/sites/images/node_43/node_51/node_230/vue-a%C3%A9rienne-paris-tour-eiffel-coucher-de-soleil-%7C-630x405-%7C-%C2%A9-fotolia/19544352-1-fre-FR/Vue-a%C3%A9rienne-Paris-Tour-Eiffel-coucher-de-soleil-%7C-630x405-%7C-%C2%A9-Fotolia.jpg',
      100,
      new Date(),
      new Date(2021, 6),
      'abc'
    ),
    new Place(
      'p3',
      'Canada',
      'This is a cold description',
      'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/1280px-Flag_of_Canada_%28Pantone%29.svg.png',
      4,
      new Date(),
      new Date(),
      'abc'
    )
  ]
   */
  private _places: BehaviorSubject<Place[]> = new BehaviorSubject<Place[]>([])
  firebaseUrl: string = 'https://ionic-angular-course-64897-default-rtdb.firebaseio.com/offered-places';
  constructor(
    private authService: AuthService,
    private http: HttpClient
  ) { }

  get places() {
    return this._places.asObservable();
  }

  fetchPlaces() {
    return this.authService.token.pipe(
      take(1),
      switchMap(token => {
        return this.http.get<{ [key: string]: PlaceData }>(this.firebaseUrl + '.json' + `?auth=${token}`);
      }),
      map(responseData => {
        const places = [];
        for (const key in responseData) {
          if (responseData.hasOwnProperty(key)) {
            places.push(
              new Place(
                key,
                responseData[key].title,
                responseData[key].description,
                responseData[key].imageUrl,
                +responseData[key].price,
                new Date(responseData[key].availableFrom),
                new Date(responseData[key].availableTo),
                responseData[key].userId,
                responseData[key].location
              )
            )
          }
        }
        return places;
      }),
      tap(places => {
        this._places.next(places);
      })
    )
  }

  getPlace(id: string) {
    return this.authService.token.pipe(
      take(1),
      switchMap(token => {
        return this.http.get<PlaceData>(this.firebaseUrl + '/' + id + '.json' + `?auth=${token}`)
      }),
      map(placeData => {
        return new Place(
          id,
          placeData.title,
          placeData.description,
          placeData.imageUrl,
          +placeData.price,
          new Date(placeData.availableFrom),
          new Date(placeData.availableTo),
          placeData.userId,
          placeData.location
        )
      })
    )
  }

  addPlace(title: string, description: string, price: number, dateFrom: Date, dateTo: Date, location: PlaceLocation, imageUrl: string) {
    let generatedId;
    let newPlace;
    let fetchedUserId: string;
    return this.authService.userId.pipe(
      take(1),
      switchMap(userId => {
        fetchedUserId = userId;
        return this.authService.token;
      }),
      switchMap(token => {
        if (!fetchedUserId)
          throw new Error('No User found!');
        newPlace = new Place(
          null,
          title,
          description,
          imageUrl,
          price,
          dateFrom,
          dateTo,
          fetchedUserId,
          location
        );
        return this.http.post<{ name: string }>(this.firebaseUrl + '.json?' + `auth=${token}`, { ...newPlace });
      }),
      switchMap(responseData => {
        generatedId = responseData.name;
        return this.places;
      }),
      take(1),
      tap(places => {
        newPlace.id = generatedId;
        this._places.next(places.concat(newPlace));
      })
    )
  }

  updatePlace(placeId: string, title: string, description: string) {
    let updatedPlaces: Place[] = [];
    let fetchedToken: string;
    return this.authService.token.pipe(
      take(1),
      switchMap(token => {
        fetchedToken = token;
        return this.places;
      }),
      take(1),
      switchMap(places => {
        if (!places || places.length <= 0) {
          return this.fetchPlaces();
        } else {
          return of(places);
        }
      }), switchMap(places => {
        let updatedPlace: Place;
        updatedPlaces = [...places].map(place => {
          if (place.id === placeId) {
            place.title = title;
            place.description = description;
          }
          updatedPlace = { ...place, id: null };
          return place;
        });
        return this.http.put(`${this.firebaseUrl}/${placeId}.json?auth=${fetchedToken}`, updatedPlace);
      }),
      tap(() => {
        this._places.next(updatedPlaces);
      })
    )
  }

  uploadImage(image: File) {
    const uploadData = new FormData()
      .append('image', image);

    return this.http.post<{ imageUrl: string, imagePath: string }>('https://us-central1-ionic-angular-course-64897.cloudfunctions.net/storeImage', uploadData)
  }
}
