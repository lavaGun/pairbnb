import { Router } from '@angular/router';
import { IonItemSliding, LoadingController, AlertController } from '@ionic/angular';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Booking } from './booking.model';
import { BookingService } from './booking.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit, OnDestroy {
  loadedBookings: Booking[];
  bookingsSub: Subscription;
  isLoading= false;
  constructor(
    private bookingService: BookingService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
    this.bookingsSub = this.bookingService.bookings.subscribe(bookings => {
      this.loadedBookings = bookings;
    }, error => {
      this.alertCtrl.create({
        header: 'An Error Occured!',
        message: 'Bookings could not be fetched. Please try again later.',
        buttons: [
          {
            text: 'Okay', handler: () => {
              this.router.navigate(['/places/tabs/discover']);
            }
          }]
      }).then(alertEl => alertEl.present())
    });
  }

  ngOnDestroy() {
    if (this.bookingsSub)
      this.bookingsSub.unsubscribe()
  }

  ionViewWillEnter() {
    this.isLoading = true;
    this.bookingService.fetchBookings().subscribe(() => {
      this.isLoading = false;
    })
  }

  onCancelBooking(bookingId: string, slidingEl: IonItemSliding) {
    slidingEl.close();
    this.loadingCtrl.create({
      message: 'Cancelling'
    }).then(loadingEl => {
      loadingEl.present();
      this.bookingService.cancelBooking(bookingId).subscribe(() => {
        loadingEl.dismiss()
      }, error => {
        this.alertCtrl.create({
          header: 'An Error Occured!',
          message: 'Booking could not be canceled. Please try again later.',
          buttons: [
            {
              text: 'Okay', handler: () => {
                this.router.navigate(['/places/tabs/discover']);
              }
            }]
        }).then(alertEl => alertEl.present())
      });
    })
  }
}
