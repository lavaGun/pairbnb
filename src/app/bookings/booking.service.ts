import { HttpClient } from '@angular/common/http';
import { AuthService } from './../auth/auth.service';
import { BehaviorSubject, generate } from 'rxjs';
import { Injectable } from '@angular/core';
import { Booking } from './booking.model';
import { take, delay, tap, switchMap, map } from 'rxjs/operators';

interface BookingData {
  id: string;
  placeId: string;
  userId: string;
  placeTitle: string;
  placeImage: string;
  guestNumber: number;
  firstName: string;
  lastName: string;
  bookedFrom: string;
  bookedTo: string;
}

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  private _bookings = new BehaviorSubject<Booking[]>([])
  firebaseUrl: string = 'https://ionic-angular-course-64897-default-rtdb.firebaseio.com/bookings';

  // Booking[] = [
  //   { id: 'xyz', placeId: 'p1', placeTitle: 'Manhattan Mansion', userId: 'abc', guestNumber: 4 }
  // ]

  constructor(
    private authService: AuthService,
    private http: HttpClient
  ) { }

  get bookings() {
    return this._bookings.asObservable();
  }

  addBooking(
    placeId: string,
    placeTitle: string,
    placeImage: string,
    firstName: string,
    lastName: string,
    guestNumber: number,
    dateFrom: Date,
    dateTo: Date
  ) {
    let fetchedUserId: string;
    let generatedId: string;
    let newBooking: Booking;
    return this.authService.userId.pipe(
      take(1),
      switchMap(userId => {
        if (!userId)
          throw new Error('No user id found!')
        fetchedUserId = userId;
        return this.authService.token;
      }),
      switchMap(token => {
        newBooking = new Booking(
          null,
          placeId,
          fetchedUserId,
          placeTitle,
          placeImage,
          guestNumber,
          firstName,
          lastName,
          dateFrom,
          dateTo
        );

        return this.http.post<{ name: string }>(this.firebaseUrl + '.json?' + `auth=${token}`, { ...newBooking })
      }),
      switchMap(res => {
        generatedId = res.name;
        return this.bookings;
      }),
      take(1),
      tap(bookings => {
        this._bookings.next(bookings.concat({ ...newBooking, id: generatedId }))
      })
    )
  }

  cancelBooking(bookingId: string) {
    return this.authService.token.pipe(
      switchMap(token => {
        return this.http.delete(`${this.firebaseUrl}/${bookingId}.json?auth=${token}`);
      }),
      switchMap(() => {
        return this.bookings;
      }),
      take(1),
      tap(bookings => {
        this._bookings.next(bookings.filter(booking => booking.id !== bookingId));
      })
    )
  }

  fetchBookings() {
    let fetchedUserId: string;
    return this.authService.userId.pipe(
      take(1),
      switchMap(userId => {
        if (!userId)
          throw new Error('Invalid user!');
        fetchedUserId = userId;
        return this.authService.token;

      }),
      take(1),
      switchMap(token => {
        return this.http.get<{ [key: string]: BookingData }>(
          `${this.firebaseUrl}.json?orderBy="userId"&equalTo="${fetchedUserId}"&auth=${token}`
        )
      }),
      map(bookingData => {
        const bookings = [];
        for (const key in bookingData) {
          if (bookingData.hasOwnProperty(key)) {
            bookings.push(
              new Booking(
                key,
                bookingData[key].placeId,
                bookingData[key].userId,
                bookingData[key].placeTitle,
                bookingData[key].placeImage,
                bookingData[key].guestNumber,
                bookingData[key].firstName,
                bookingData[key].lastName,
                new Date(bookingData[key].bookedFrom),
                new Date(bookingData[key].bookedTo)
              )
            )
          }
        }
        return bookings;
      }),
      tap(bookings => {
        this._bookings.next(bookings);
      })
    )

  }
}
